/**
 * Created by Alexey on 11/14/2015.
 */

var _diff_obj = function(params) {

    var from = params.from;
    var to = params.to;
    var map = params.map || {};
    var ignore_keys = params.ignore_keys || [];
    var global_path = params.global_path || '';
    var apply = params.apply || false;
    var local_path;

    for (var key in from) {
        if (!from.hasOwnProperty(key) || ignore_keys.indexOf(key) >= 0) continue;
        local_path = global_path + '/' + key;
        if (to[key] != undefined) {
            params = {
                from: from[key],
                to: to[key],
                map: map,
                ignore_keys: ignore_keys,
                global_path: local_path,
                apply: apply
            };
            // check if it's object or array
            if (Object.prototype.toString.call(from[key]) === '[object Object]' &&
                Object.prototype.toString.call(to[key]) === '[object Object]') {
                _diff_obj(params);
            }
            else if (Object.prototype.toString.call(from[key]) === '[object Array]' &&
                Object.prototype.toString.call(to[key]) === '[object Array]') {
                _diff_arr(params);
            }
            else if (from[key] != to[key]) {
                map[local_path] = {action: 'update', from: from[key], to: to[key]};
                if (apply) from[key] = to[key];
            }
        } else {
            map[local_path] = {action: 'delete', from: from[key], to: null};
            if (apply) delete from[key];
        }
    }

    var to_keys = Object.keys(to);
    var from_keys = Object.keys(from);
    var new_keys = to_keys.filter(function(el) {
        return from_keys.indexOf(el) < 0;
    });

    for (i = 0; i < new_keys.length; i++) {
        local_path = global_path + '/' + new_keys[i];
        map[local_path] = {action: 'add', from: null, to: to[new_keys[i]]};
        if (apply) from[new_keys[i]] = to[new_keys[i]];
    }
    return map;
};

var _diff_arr = function(params) {

    var from = params.from;
    var to = params.to;
    var map = params.map || {};
    var ignore_keys = params.ignore_keys || [];
    var global_path = params.global_path || '';
    var apply = params.apply || false;
    var min = Math.min(from.length, to.length);
    var local_path;

    for (var i = 0; i < min; i++) {
        local_path = global_path + '[' + i + ']';
        params = {
            from: from[i],
            to: to[i],
            map: map,
            ignore_keys: ignore_keys,
            global_path: local_path,
            apply: apply
        };
        if (Object.prototype.toString.call(from[i]) === '[object Object]' &&
            Object.prototype.toString.call(to[i]) === '[object Object]') {
            _diff_obj(params);
        }
        else if (Object.prototype.toString.call(from[i]) === '[object Array]' &&
            Object.prototype.toString.call(to[i]) === '[object Array]') {
            _diff_arr(params);
        }
        else if (from[i] != to[i]) {
            map[local_path] = {action: 'update', from: from[i], to: to[i]};
            if (apply) from[i] = to[i];
        }
    }
    if (from.length - min > 0) {
        for (i = to.length; i < from.length; i++) {
            local_path = global_path + '[' + i + ']';
            map[local_path] = {action: 'delete', from: from[i], to: null};
        }
        if (apply) from.splice(to.length, from.length - min);
    }

    if (to.length - min > 0) {
        for (i = from.length; i < to.length; i++) {
            local_path = global_path + '[' + i + ']';
            map[local_path] = {action: 'add', from: null, to: to[i]};
            if (apply) from.push(to[i]);
        }
    }
    return map;
};

var diff = function(params) {
    if (!params.from || !params.to) {
        return new Error('Either "from" or "to" object is missing in params.');
    }

    if (Object.prototype.toString.call(params.from) != Object.prototype.toString.call(params.to)) {
        return new Error('Both "from" and "to" objects have to be of the same type (objects or arrays).');
    }

    if (Object.prototype.toString.call(params.from) === '[object Object]') {
        return _diff_obj(params);
    } else {
        return _diff_arr(params);
    }
};

module.exports = diff;