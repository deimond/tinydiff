# tinydiff

Simple tool which builds a map of changes between two objects or arrays. Allows to apply changes to the original object, which might be useful if you need to update a mongodb document, but would like to take advantage of mongoose presave hooks. Supports nested objects just fine.

## Installation

npm install tinydiff

## Usage


```
#!javascript

var diff = require("tinydiff");

var user1 = {
    "username" : "admin",
    "password" : "$2a$10$Rrwf5HdksNk3pMiOE03Rje2.L.xaSfCDOMfoAjaaU8NmlaZdRb2ju",
    "first_name" : "Super",
    "last_name" : "User",
    "email" : "superuser@gmail.com",
    "role" : "superuser",
    "modified" : "2015-11-12T04:18:35.378Z",
    "sites" : ['site1', 'site2'],
    "oldTestObj": {'key1': 'value1'},
    "__v" : 0
};

var user2 = {
    "username" : "admin",
    "password" : "someOtherCrazyHash",
    "first_name" : "Super",
    "last_name" : "User",
    "email" : "superuser@yahoo.com",
    "role" : "superuser",
    "modified" : "2015-11-12T04:18:35.378Z",
    "sites" : ['site1', 'site3', 'site4'],
    "newTestObj": {'key2': 'value2'},
    "__v" : 0
};

console.log(diff({from: user1, to: user2, ignore_keys: ['password', 'modified'], apply: false}));
```

Result:


```
#!javascript

{ '/email': 
   { action: 'update',
     from: 'superuser@gmail.com',
     to: 'superuser@yahoo.com' },
  '/sites[1]': { action: 'update', from: 'site2', to: 'site3' },
  '/sites[2]': { action: 'add', from: null, to: 'site4' },
  '/oldTestObj': { action: 'delete', from: { key1: 'value1' }, to: null },
  '/newTestObj': { action: 'add', from: null, to: { key2: 'value2' } } }
```

* from - original object, required.
* to - updated object, required.
* ignore_keys - object keys to ignore for comparison and applying steps, optional. Default - [].
* apply - tells to apply the changes to the original object to match it with the updated one, optional. Default - false.

## History

* version 0.0.1 - initial commit.
* version 0.0.2 - fixed a typo (quotes around the word undefined).

## License

MIT